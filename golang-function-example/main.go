package main

import (
	"context"
	"fmt"
	"log"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-lambda-go/lambdacontext"
)

type MyEvent struct {
	Name string `json:"What is your name?"`
	Age  int    `json:"How old are you?"`
}

type MyResponse struct {
	LambdaFunctionName string `json:"lambdaFunctionName"`
	Message            string `json:"answer"`
}

func HandleLambdaEvent(ctx context.Context, event *MyEvent) (*MyResponse, error) {
	lc, _ := lambdacontext.FromContext(ctx)
	log.Printf("InvokedFunctionArn: %s", lc.InvokedFunctionArn)
	log.Printf("Function name: %s", lambdacontext.FunctionName)

	if event == nil {
		return nil, fmt.Errorf("received nil event")
	}

	response := &MyResponse{Message: fmt.Sprintf("%s is %d years old!", event.Name, event.Age), LambdaFunctionName: lambdacontext.FunctionName}

	return response, nil
}

func main() {
	lambda.Start(HandleLambdaEvent)
}
