aws lambda create-function --function-name golangFunctionExample \
--runtime provided.al2023 --handler bootstrap \
--architectures x86_64 \
--role arn:aws:iam::510577403500:role/lambda-function \
--zip-file fileb://golangFunctionExample.zip
